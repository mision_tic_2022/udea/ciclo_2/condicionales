
package com.mycompany.condicionales;

import java.util.Scanner;

public class Condicionales {

    public static void main(String[] args) {
        int n1 = 10;
        int n2 = 20;
        /*
         * && -> and -> y
         * || -> or -> o
         * ! -> not -> negado
         */
        if( n1 > 0 && n1 < 100 ||  n2 > 10 && n2 < 30 ){
            System.out.println("Cumple la condición");
        }

        //diferente que
        if( n1 != 10 ){
            System.out.println("n1 es diferente a 10");
        }else{
            System.out.println("n1 es igual a 10");
        }

        //Negar
        if( !(n2 > 20 && n1 < 10) ){
            System.out.println("Cumple la condición");
        }else{
            System.out.println("No cumple la condición");
        }
        
        condicional_cascada(3);
        Double resp = calcular_operacion(10.0, 2.0, "+");
        System.out.println("Resultado de sumar -> "+resp);
        resp = calcular_operacion(10.0, 2.0, "*");
        System.out.println("Resultado de multiplicar -> "+resp);
        resp = calcular_operacion(10.0, 2.0, "-");
        System.out.println("Resultado de restar -> "+resp);
        resp = calcular_operacion(10.0, 2.0, "/");
        System.out.println("Resultado de dividir -> "+resp);
    }

    public static void condicional_cascada(int n){
        if(n > 0 && n <= 5){
            if(n == 1){
                System.out.println("Es igual a 1");
            }else if(n == 2){
                System.out.println("Es igual a 2");
            }else if(n == 3){
                System.out.println("Es igual a 3");
            }else if(n == 4){
                System.out.println("Es igual a 4");
            }else{
                System.out.println("Es igual a 5");
            }
        }
    }

    /**
     * EJERCICIO #1
     * Desarrolle una función que reciba como parámetro dos números
     * decimales y un string.
     * El parámetro String determina la operación a aplicar a los números 
     * Ej: * / + -
     * Ejecute la operación correspondiente y retorne el resultado
     */
    public static Double calcular_operacion(Double n1, Double n2, String operador){
        //Variable que representa el resultado de la operación
        Double resultado = 0.0;
        //Evaluar operador
        if(operador == "+"){
            resultado = n1+n2;
        }else if(operador == "-"){
            resultado = n1-n2;
        }else if(operador == "*"){
            resultado = n1*n2;
        }else if(operador == "/"){
            resultado = n1/n2;
        }
        //Retornar el resultado de la operación
        return resultado;
    }

    /**
      * EJERCICIO #2
      * Desarrolle una función que SOLICITE POR TECLADO un número del 
      * 1 al 6 y muestre en pantalla el mes relacionado a ese número.
      * Ejemplo:
      * 1 -> Enero
      * 2 -> Febrero
      * 3 -> Marzo
      * ...
      * 6 -> Junio
      */
    public static void solicitar_mes(){
        try(Scanner leer = new Scanner(System.in)){
            //Mostrar mensaje
            System.out.print("Ingrese número del mes (1 a 6): ");
            //Queda a la escucha de lo que ingrese el usuario
            int mes = leer.nextInt();
            //Validar que el número del mes esté dentro del rango
            if(mes >= 1 && mes <= 6){
                if(mes == 1){
                    System.out.println("Enero");
                }else if(mes == 2){
                    System.out.println("Febrero");
                }else if(mes == 3){
                    System.out.println("Marzo");
                }else if(mes == 4){
                    System.out.println("Abril");
                }else if(mes == 5){
                    System.out.println("Mayo");
                }else if(mes == 6){
                    System.out.println("Junio");
                }
            }else{
                System.out.println("Debe ingresar un número dentro del rango de 1 a 6");
            }

        }catch(Exception error){
            System.out.println(error);
        }
    }

      /**
       * EJERCICIO #3
       * Desarrolle una función calcule y retorne el subsidio de una persona.
       * Si la persona tiene 1 hijo recibirá un subsidio de $500.000,
       * si son 2 hijos $1200.000, si son 3 hijos $1800.000, si tiene 
       * 4 o más hijos recibirá $3'200.000
       */
}
